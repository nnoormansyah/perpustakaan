from pymongo import MongoClient
from bson.objectid import ObjectId
from books import database as db
import csv

def add_book():
    data = open(r"C:\Users\Asus\Downloads\mysql\mongoDB\bestsellers-with-categories.csv", encoding='utf-8')
    books = csv.reader(data, delimiter=',')
    next(books)

    for book in books:
        try:
            data = {
                "nama": book[0],
                "pengarang": book[1],
                "tahunterbit": book[5],
                "genre": book[6]
            }
            db.insertBook(data)
        except Exception as e:
            print("Kesalahan pada saat memasukan data: {}".format(e))
            break

def search_books(params):
    for book in db.searchBookByName(params):
        print(book)

def show_books():
    for book in db.showBooks():
        print(book)

def show_booksId(id):
    book = db.showBookById(id)
    print(book)

def update_book(id, field, value):
    db.updateBookById(id, field, value)
    print('Buku berhasil diupdate')

def delete_book(id):
    db.deleteBookById(id)
    print('Buku berhasil dihapus')

if __name__ == "__main__":
    db = db()
    add_book()
    #delete_book("606c029d2c8914ffeb4b8799")
    #update_book("606c029d2c8914ffeb4b8799", "pengarang", "doppant")
    #show_booksId("606c029d2c8914ffeb4b8799")
    #search_books('606c029d2c8914ffeb4b8799')
    db.nosql_db.close()