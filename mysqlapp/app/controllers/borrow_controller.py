from app.models.borrow_model import database
from app.models.customer_model import database as cust_db
from flask import jsonify, request
from flask_jwt_extended import *
import json, datetime, requests

mysqldb = database()
cust_db = cust_db()

@jwt_required()
def shows():
    params = get_jwt_identity()
    dbresult = mysqldb.showBorrowByEmail(**params)
    result = []
    if dbresult is not None:
        for item in dbresult:
            id = json.dumps({"id":item[4]})
            bookdetails = getBookbyId(id)
            user = {
                "username":item[0],
                "borrowid":item[1],
                "borrowdate":item[2],
                "bookid":item[4],
                "bookname":item[5],
                "author": bookdetails["pengarang"],
                "releaseyear": bookdetails["tahunterbit"],
                "genre": bookdetails["genre"]
            }
            result.append(user)
    else:
        result = dbresult
    return jsonify(result)

@jwt_required()
def changeStatus(**params):
    mysqldb.updateBorrow(**params)
    mysqldb.dataCommit()
    return jsonify({"messege":"Success"})

@jwt_required()
def insert(**params):
    token = get_jwt_identity()
    userid = cust_db.showUserByEmail(**token)[0]
    borrowdate = datetime.datetime.now().isoformat()
    id = json.dumps({"id":params["bookid"]})
    bookname = getBookbyId(id)["nama"]
    params.update({
        "userid":userid,
        "borrowdate":borrowdate,
        "bookname":bookname,
        "isactive":1
        })
    mysqldb.insertBorrow(**params)
    mysqldb.dataCommit()
    return jsonify({"messege":"Success"})

def getBookbyId(data):
    book_data = requests.post(url = "http://localhost:8000/bookbyid", data=data)
    return book_data.json()