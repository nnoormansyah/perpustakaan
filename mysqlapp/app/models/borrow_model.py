from mysql.connector import connect

class database:
    def __init__(self):
        try:
            self.db = connect(host = 'localhost',
                            database = 'perpustakaan',
                            user = 'root',
                            password = '')
        except Exception as e:
            print(e)

    def showBorrowByEmail(self, **params):
        cursor = self.db.cursor()
        query = '''
        SELECT customers.username, borrows.*
        FROM borrows
        inner join customers on borrows.userid = customers.userid
        WHERE customers.email = "{0}" and borrows.isactive = 1;
        '''.format(params["email"])
        cursor.execute(query)
        result = cursor.fetchall()
        return result

    def insertBorrow(self, **params):
        column = ', '.join(list(params.keys()))
        values = tuple(list(params.values()))    
        cursor = self.db.cursor()
        query = '''
        INSERT INTO borrows ({0})
        VALUES {1};
        '''.format(column, values)
        cursor.execute(query)
        

    def updateBorrow(self, **params):
        borrowid = params["borrowid"]
        cursor = self.db.cursor()
        query = '''
        UPDATE borrows
        SET isactive = 0
        WHERE borrowid = {0}
        '''.format(borrowid)
        cursor.execute(query)

    def dataCommit(self):
        self.db.commit()