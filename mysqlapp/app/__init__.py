from flask import Flask
from config import Config
from flask_jwt_extended import JWTManager


app = Flask(__name__)
app.config.from_object(Config)
jwt = JWTManager(app)

from app.routers.customer_router import *
from app.routers.borrow_router import *

app.register_blueprint(customer_blueprint)
app.register_blueprint(borrow_blueprint)