# Dokumentasi
Aplikasi ini merupakan sebuah aplikasi perpustakaan dengan menggunakan RestAPI dan FastAPI. Aplikasi menggunakan tiga  database yaitu **database** **customer**, **database** **book** dan **database** **borrow**. 
***
## Requirement
```python
pip install fastapi
pip install uvicorn
pip install flask
pip install pymongo
pip install mysql-connector-python
```
***

## How to start
#### Menjalankan mongodb fastapi
```bash
uvicorn main:app --reload
```
### Menjalankan mysql restapi
```bash
python main.py
```

***

Beberapa hal yang bisa dilakukan dalam aplikasi ini adalah sebagai berikut:
### Customer
#### Melakukan request token
http://127.0.0.1:5000/user/token
``masukkan nilai email``
``paramas = {"email":"agoes@mail.com"}``


#### Melihat semua user
http://127.0.0.1:5000/users


#### Melihat user dengan id
http://127.0.0.1:5000/user
``params = {"id":1}``


#### Menambahkan user
http://127.0.0.1:5000/user/insert
`paramas = {"username":"agusimoet",`
            `"namadepan":"agus",`
            `"namabelakang":"agoes",`
            `"email":"agoes@mail.com"}`


#### Melakukan perubahan terhadap data user berdasarkan id
http://127.0.0.1:5000/user/update
``masukkan nilai parameter yang ingin diubah``
`paramas = {"id":1,`
            `"username":"agusimoet",`
            `"namadepan":"agus",`
            `"namabelakang":"agoes",`
            `"email":"agoes@mail.com"}`


#### Menghapus user berdasarkan id
http://127.0.0.1:5000/user/update
`masukkan nilai id yang ingin dihapus`
`paramas = {"id":1}`

***

### Borrow
#### Melihat semua user yang melakukan peminjaman
http://127.0.0.1:5000/borrows
`gunakan auth **bearer token** yang didapatkan dari request token`


#### Menambahkan user yang melakukan peminjaman
http://127.0.0.1:5000/borrows/insert
`gunakan auth **bearer token** yang didapatkan dari request token`
`params = {"borrowdate":,`
            `"userid":,`
            `"bookid",`
            `"bookname",`
            `"isactive":}`


#### Merubah status pinjaman pengembalian buku
http://127.0.0.1:5000/borrows/status
`gunakan auth **bearer token** yang didapatkan dari request token`
`params = {"borrowid":}`

***

## Books
#### Melihat semua buku
http://127.0.0.1:8000/books


#### Melihat buku berdasarkan id
http://127.0.0.1:8000/bookbyid
`params = {"id":}`


#### Melihat buku berdasarkan nama buku
http://127.0.0.1:8000/bookbyname
`params = {"nama":}`